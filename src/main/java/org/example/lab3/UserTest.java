package org.example.lab3;

import com.github.javafaker.Faker;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.apache.hc.core5.http.HttpStatus;
import org.openqa.selenium.remote.Response;
import org.testng.annotations.BeforeClass;

import io.restassured.RestAssured;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;

import java.util.Map;

import static com.google.common.base.Predicates.equalTo;
import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;


public class UserTest {

    private static final String baseUrl = "https://petstore.swagger.io/v2";
    private static final String PET = "/pet";

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = baseUrl;
    }

    @Test
    public void createPet() {
        String petName = "Cat";
        long petId = 12345;

        String jsonBody = String.format("{ \"id\": %d, \"name\": \"%s\" }", petId, petName);

        Response response = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(jsonBody)
                .post(PET);

        assertEquals(response.getStatusCode(), 200);
    }

    @Test(dependsOnMethods = "createPet")
    public void getPet() {
        long petId = 12345;

        Response response = RestAssured.get(PET + "/" + petId);

        assertEquals(response.getStatusCode(), 200);
        assertEquals(response.jsonPath().get("name"), "Cat");
    }

    @Test(dependsOnMethods = "getPet")
    public void deletePet() {
        long petId = 12345;

        Response response = RestAssured.delete(PET + "/" + petId);

        assertEquals(response.getStatusCode(), 200);
    }



    }


