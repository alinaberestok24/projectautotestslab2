package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class AmazonTest {

    private WebDriver chromeDriver;

    private static final String baseUrl = "https://www.amazon.com/";

    @BeforeClass(alwaysRun = true)
    public void setUp() {
        WebDriverManager.chromedriver().setup();

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-maximized");
        chromeOptions.setPageLoadTimeout(Duration.ofSeconds(30));
        chromeOptions.setScriptTimeout(Duration.ofSeconds(15));

        this.chromeDriver = new ChromeDriver(chromeOptions);
    }

    @BeforeMethod
    public void preconditions() {
        chromeDriver.get(baseUrl);
    }

    @Test
    public void testSearchProduct() {
        WebElement searchField = chromeDriver.findElement(By.id("twotabsearchtextbox"));
        Assert.assertNotNull(searchField);

        String inputValue = "Smartphone";
        searchField.sendKeys(inputValue);
        Assert.assertEquals(searchField.getAttribute("value"), inputValue);

        searchField.sendKeys(Keys.ENTER);
    }

    @Test
    public void testSearchFieldOnProductPage() {
        chromeDriver.get("https://www.amazon.com/dp/B07V2BBJKK");

        WebElement searchField = chromeDriver.findElement(By.id("product-search-field"));

        Assert.assertNotNull(searchField);

        String inputValue = "Accessories";
        searchField.sendKeys(inputValue);
        Assert.assertEquals(searchField.getAttribute("value"), inputValue);

        searchField.sendKeys(Keys.ENTER);

        WebElement searchResults = chromeDriver.findElement(By.id("search-results"));

        Assert.assertTrue(searchResults.isDisplayed());
    }


    @Test
    public void testSlider() {
        WebElement nextButton = chromeDriver.findElement(By.id("nextButton"));
        WebElement prevButton = chromeDriver.findElement(By.id("prevButton"));

        Assert.assertNotNull(nextButton);
        Assert.assertNotNull(prevButton);

        for (int i = 0; i < 5; i++) {
            nextButton.click();
        }

        for (int i = 0; i < 5; i++) {
            prevButton.click();
        }
    }


    @AfterClass(alwaysRun = true)
    public void tearDown() {
        chromeDriver.quit();
    }



}
