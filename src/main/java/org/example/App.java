package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import java.time.Duration;

public class App {

    private WebDriver chromeDriver;

    private static final String baseUrl = "https://www.nmu.org.ua/ua/";

    @BeforeClass(alwaysRun = true)
    public void setUp() {

        WebDriverManager.chromedriver().setup();

        ChromeOptions chromeOptions = new ChromeOptions();

        chromeOptions.addArguments("--start-fullscreen");

        chromeOptions.setImplicitWaitTimeout(Duration.ofSeconds(15));

        this.chromeDriver = new ChromeDriver(chromeOptions);

        System.setProperty("webdriver.chrome.driver", "\\chromedriver.exe");
        chromeDriver = new ChromeDriver();
    }

    @BeforeMethod
    public void preconditions(){
        //open main page
        chromeDriver.get(baseUrl);
    }

    @Test
    public void testHeaderExists(){
        WebElement header = chromeDriver.findElement(By.id("header"));
        Assert.assertNotNull(header);
    }

    @Test
    public void testClickOnForStudent() {
        WebElement forStudentButton = chromeDriver.findElement(By.xpath("/html/body/center/div[4]/div/div[1]/ul/li[4]/a"));
        Assert.assertNotNull(forStudentButton);
        forStudentButton.click();
        Assert.assertNotEquals(chromeDriver.getCurrentUrl(), baseUrl);
    }

    @Test
    public void testSearchFieldOnForStudentPage() {
        String studentPageUrl = "content/student_file/students/";
        chromeDriver.get(baseUrl + studentPageUrl);
        WebElement searchField = chromeDriver.findElement(By.tagName("input"));
        Assert.assertNotNull(searchField);

        System.out.println( String.format("Name attribute: %s", searchField.getAttribute("name")) +
                   String.format("\n ID attribute: %s", searchField.getAttribute("id")) +
                   String.format("\n Type attribute: %s", searchField.getAttribute("type")) +
                   String.format("\n Value attribute: %s", searchField.getAttribute("value")) +
                   String.format("\n Position: (%d;%d)", searchField.getLocation().x, searchField.getLocation().y) +
                   String.format("\n Size: (%dx;%d)", searchField.getSize().height, searchField.getSize().width)
        );
        String inputValue = "I need info";
        searchField.sendKeys(inputValue);
        Assert.assertEquals(searchField.getText(), inputValue);
        searchField.sendKeys(Keys.ENTER);
        Assert.assertNotEquals(chromeDriver.getCurrentUrl(), studentPageUrl);
    }

    @Test
    public void testSlider() {
        WebElement nextButton = chromeDriver.findElement(By.className("next"));
        WebElement nextButtonByCss = chromeDriver.findElement(By.cssSelector("a.next"));

        Assert.assertEquals(nextButton, nextButtonByCss);

        WebElement previousButton = chromeDriver.findElement(By.className("prev"));

        for (int i = 0; i < 20; i++){
            if(nextButton.getAttribute("class").contains("disabled")){
                previousButton.click();
                Assert.assertTrue(previousButton.getAttribute("class").contains("disabled"));
                Assert.assertFalse(nextButton.getAttribute("class").contains("disabled"));
            } else {
                nextButton.click();
                Assert.assertTrue(nextButton.getAttribute("class").contains("disabled"));
                Assert.assertFalse(previousButton.getAttribute("class").contains("disabled"));
            }
        }
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
       chromeDriver.quit();
    }
}



